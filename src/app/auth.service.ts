/*
service to control if user has been loged
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url: string = "http://dev.contanimacion.com/birds/public/login/";

  constructor(
    private httpClient: HttpClient,
    private router: Router
    ) { }

  body: string = "";

  /*
  this var is the core of the authentication
   */
  isLoggedIn: boolean = false;

  /*
  this sends the user and password to login
   */
  getLogin(user:string, password: string):Observable<any> {
    return this.httpClient.post<any[]>(this.url + "?user=" + user + "&password=" + password , this.body);
  }

  /*
  method to know if user is logged or not
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.isLoggedIn) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
