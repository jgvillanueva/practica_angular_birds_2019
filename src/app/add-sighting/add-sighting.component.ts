import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

import { BirdsApiService } from '../birds-api.service';


@Component({
  selector: 'app-add-sighting',
  templateUrl: './add-sighting.component.html',
  styleUrls: ['./add-sighting.component.scss']
})
export class AddSightingComponent implements OnInit {

  @ViewChild('addSightingForm') addSightingForm: NgForm;

  latitude_sighting: number;
  longitude_sighting: number;
  idAve: number;
  place:string = '';
  coordsOK: boolean = false;
  spinnerCoordsOn: boolean = false;
  spinnerSightingOn: boolean = false;


  private subscription:any;
  
  constructor(
    private birdsApiService: BirdsApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _location: Location
    ) { }
  
  /*
  back button click function
   */
  backClicked() {
    this._location.back();
  }

  /*
  get location
   */
  geoPosition () {
    if (navigator.geolocation) {
      this.spinnerCoordsOn = true;
      setTimeout(() => {
        navigator.geolocation.getCurrentPosition((data) => {
          console.log('****', data);
          this.latitude_sighting = data.coords.longitude;
          this.longitude_sighting = data.coords.latitude;
          this.coordsOK = true;
          this.spinnerCoordsOn = false;
        });
      },100);

    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  /*
  send form
   */
  onSubmit() {
    /*
    call to service
     */
    this.birdsApiService.addSighting(this.idAve, this.place, this.latitude_sighting, this.longitude_sighting)
      .subscribe(data => {
        if( data.status == 'OK' ){
          alert("Avistamiento añadido.");
          this._location.back();
        } else if ( data.status == 'KO' ) {
          alert("SERVER ERROR:\nServer says:\n"+ data.message);
        }
      },
      err => {
        alert("ERROR IN REQUEST TO SERVER:\n"+ err);
      }
    );
  }

  /*
  load location on init
   */
  ngOnInit() {
    this.subscription = this.activatedRoute.params.subscribe(params => {
      this.idAve = +params['id']; // (+) convierte string 'id' a number

      this.geoPosition();
    });
  }
}
