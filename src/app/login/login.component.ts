import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot  } from '@angular/router';

import { BirdsApiService } from '../birds-api.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  /*
  with @ViewChild we have access to the form
   */
  @ViewChild('loginForm') loginForm: NgForm;


  userName: string;
  userPassword: string;
  idUser: string;
  isLoggedIn: boolean = false;
  spinnerLoginOn: boolean = false;

  constructor(
    private birdsService: BirdsApiService,
    private authService: AuthService,
    public router: Router
  ) { }
  
  /*
  send the form to login
   */
  onSubmit() {
    /*
    show spinner
     */
    this.spinnerLoginOn = true;
    /*
    call to the service to send login
     */
    this.birdsService.getLogin(this.userName, this.userPassword)
      .subscribe(data => {
        /*
        receive the promise and use the response
         */
        if( data.status == 'OK' ) {
          this.idUser = data.id;
          this.birdsService.saveIdUser(this.idUser);
          /*
          change the isloggedIn value in auth to know we are inside
           */
          this.authService.isLoggedIn = true;
          /*
          all ok: we go home
           */
          this.router.navigate(['/birds-listing']);
        } else if ( data.status == 'KO' ) {
          alert("SERVER ERROR:\nServer says:\n"+ data.message);
        }
        /*
        close spinner
         */
        this.spinnerLoginOn = false;
      },
      err => {
        alert("ERROR IN REQUEST TO SERVER:\n"+ err);
        /*
        close spinner
         */
        this.spinnerLoginOn = false;
      }
    );
  }

  ngOnInit() {

  }

}
