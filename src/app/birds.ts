/*
definition of birds and sigthings
 */
export interface Birds {
    id: number;
    bird_image: string;
    bird_name: string;
    bird_description: string;
    bird_sightings: number;
    sightings_list: Array<Sigthing>;
    mine: number;
}

export interface Sigthing {
    id: number;
    place: string;
    long: number;
    lat: number;
}
