/*
service to connect with the API
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Birds } from './birds'

@Injectable({
  providedIn: 'root'
})
  
export class BirdsApiService {

  public idUser: string;
  body: string = "";

  /*
  url of service
   */
  private urlBirdsApi: string = "http://dev.contanimacion.com/birds/public/";

  constructor(private httpClient: HttpClient) { }

  /*
  this returns a promise with bird list
   */
  getLogin(user:string, password: string): Observable<any> {
    console.log(this.urlBirdsApi + "login/?user=" + user + "&password=" + password , this.body);
    return this.httpClient.post<any[]>(this.urlBirdsApi + "login/", {user: user,  password: password});
  }

  /*
  saves user id to use it later
   */
  saveIdUser(id: string) {
    this.idUser = id;
    console.log(this.idUser);
  }

  /*
  get detail by bird id
   */
  getBirdById( id: number ):Observable<any> {
    return this.httpClient
    .get<Birds[]>(this.urlBirdsApi + "getBirdDetails/" + id);
  }

  /*
  get all birds
   */
  getBirds():Observable<any> {
    return this.httpClient
    .get<Birds[]>(this.urlBirdsApi + "getBirds/" + this.idUser);
  }

  /*
  adds new bird with POST
   */
  addBird(name: string, description: string, place: string, lng: number, lat:number):Observable<any> {
    const body = {
      idUser: this.idUser,
      bird_name: name,
      bird_description: description,
      place: place,
      long: lng,
      lat: lat
    }
    console.log(this.urlBirdsApi + "addBird/" + this.idUser, body);
    return this.httpClient
    .post<any>(this.urlBirdsApi + "addBird/", body);
  }

  /*
  add new sighting with POST
   */
  addSighting(idAve: number, place: string, lng: number, lat: number):Observable<any> {
    const body = {
      idAve: idAve,
      place: place,
      long: lng,
      lat: lat
    }
    console.log(this.urlBirdsApi + "addSighting/", body);
    return this.httpClient
    .post<any>(this.urlBirdsApi + "addSighting/", body);
  }
}
