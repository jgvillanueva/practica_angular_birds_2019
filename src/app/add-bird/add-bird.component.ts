import { Component, ViewChild, OnInit } from '@angular/core';
import {  Router } from "@angular/router";
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

import { BirdsApiService } from '../birds-api.service';

@Component({
  selector: 'app-add-bird',
  templateUrl: './add-bird.component.html',
  styleUrls: ['./add-bird.component.scss']
})

export class AddBirdComponent implements OnInit {
  @ViewChild('addBirdForm') addBirdForm: NgForm;

  bird_name: string;
  bird_description: string;
  /*
  this is used to show more fields
   */
  bird_seen: boolean;
  bird_place: string;
  latitude_sighting: number;
  longitude_sighting: number;
  spinnerAddOn: boolean = false;
  spinnerLatOn: boolean = false;


  constructor(
    private router: Router,
    private birdsApiService: BirdsApiService,
    private _location: Location
    ) { }

  ngOnInit() {
  }

  onSubmit() {
    /*
    show spinner
     */
    this.spinnerAddOn = true;
    /*
    send form  data
     */
    this.birdsApiService.addBird(this.bird_name, this.bird_description, this.bird_place, this.latitude_sighting, this.longitude_sighting)
      .subscribe(data => {
        if( data.status == 'OK' ){
          alert("Pájaro añadido.");
          this.spinnerAddOn = false
          this.router.navigate(['/birds-listing']);
        } else if ( data.status == 'KO' ) {
          alert("SERVER ERROR:\nServer says:\n"+ data.message);
        }
      },
      err => {
        alert("ERROR IN REQUEST TO SERVER:\n"+ err);
      }
    );
    /*
    hide spinner
     */
    this.spinnerAddOn = false;
  }

  /*
  back button click function
   */
  backClicked() {
    this._location.back();
  }
  
  /*
  get location
   */
  geoPosition () {
    this.spinnerLatOn = true;
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        position => {
          this.latitude_sighting = position.coords.latitude;
          this.longitude_sighting = position.coords.longitude;
        },
        error => {
          switch (error.code) {
            case 1:
                console.log('Position permission denied');
                break;
            case 2:
                console.log('Position Unavailable');
                break;
            case 3:
                console.log('Timeout');
                break;
          }
        }
      );
    }
    this.spinnerLatOn = false;
  }
}
