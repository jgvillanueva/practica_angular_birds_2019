import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

/*
Every component used in application
 */
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { BirdsListingComponent } from './birds-listing/birds-listing.component';
import { BirdDetailComponent } from './bird-detail/bird-detail.component';
import { AddBirdComponent } from './add-bird/add-bird.component';
import { AddSightingComponent } from './add-sighting/add-sighting.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { BirdsApiService } from './birds-api.service';

/*
needed to get data from birds remote service
 */
import { HttpClientModule } from '@angular/common/http';

/*
needed to use angular forms
 */
import { FormsModule } from '@angular/forms';

@NgModule({
  /*
  decalarations needed to use components
   */
  declarations: [
    AppComponent,
    MenuComponent,
    BirdsListingComponent,
    BirdDetailComponent,
    AddBirdComponent,
    AddSightingComponent,
    LoginComponent
  ],
  /*
  imports needed to use code in controllers, templates, ...
   */
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  /*
  here are the services used in application
   */
  providers: [
    AuthService, 
    BirdsApiService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
