/*
this service creates all of the routes
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuComponent } from './menu/menu.component';
import { BirdsListingComponent } from './birds-listing/birds-listing.component';
import { BirdDetailComponent } from './bird-detail/bird-detail.component';
import { AddBirdComponent } from './add-bird/add-bird.component';
import { AddSightingComponent } from './add-sighting/add-sighting.component';
import { LoginComponent } from './login/login.component';

import { AuthService } from './auth.service';

/*
this is the array of routes
 */
const routes: Routes = [
  // Home
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'menu', component: MenuComponent, canActivate: [AuthService]},
  { path: 'birds-listing', component: BirdsListingComponent, canActivate: [AuthService] },
  // this route needs param id to use it to know the id of the bird
  { path: 'bird-detail/:id', component: BirdDetailComponent, canActivate: [AuthService] },
  { path: 'add-bird', component: AddBirdComponent, canActivate: [AuthService] },
  { path: 'add-sighting/:id', component: AddSightingComponent, canActivate: [AuthService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
