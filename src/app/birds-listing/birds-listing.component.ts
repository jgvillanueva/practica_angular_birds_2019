import { Component, OnInit } from '@angular/core';
import { BirdsApiService } from '../birds-api.service';
import { Location } from '@angular/common';


import { Birds } from '../birds'

@Component({
  selector: 'app-birds-listing',
  templateUrl: './birds-listing.component.html',
  styleUrls: ['./birds-listing.component.scss']
})
export class BirdsListingComponent implements OnInit {
  birds:Birds[];
  spinnerListingOn: Boolean = false;

  constructor(
    private birdsApiService: BirdsApiService,
    private _location: Location
  ) { }

  /*
  back to list button click function
   */
  backClicked() {
    this._location.back();
  }
  
  /*
  in the init of the component we need to get the detail of the bird
  we use the service to get data
   */
  ngOnInit() {
    /*
    show spinner
     */
    this.spinnerListingOn = true;
    this.birdsApiService.getBirds()
      .subscribe(data => {
        // Gestionamos la respuesta de la API birds
        if( data.status == 'KO' ) {
          alert("SERVER ERROR:\nServer says:\n"+ data.message);
        } else this.birds = data;

        this.spinnerListingOn = false;
      },
      err => {
        alert("ERROR IN REQUEST TO SERVER:\n"+ err);
        this.spinnerListingOn = false;
      }   
    );
  }
}
