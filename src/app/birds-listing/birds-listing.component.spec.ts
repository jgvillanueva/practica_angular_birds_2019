import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirdsListingComponent } from './birds-listing.component';

describe('BirdsListingComponent', () => {
  let component: BirdsListingComponent;
  let fixture: ComponentFixture<BirdsListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirdsListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirdsListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
