import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';

import { BirdsApiService } from '../birds-api.service';
import { Birds } from '../birds'

@Component({
  selector: 'app-bird-detail',
  templateUrl: './bird-detail.component.html',
  styleUrls: ['./bird-detail.component.scss']
})
export class BirdDetailComponent implements OnInit {

  private _subscription:any;

  id:number;
  bird:Birds;
  spinnerBirdOn: Boolean = false;

  constructor(
    private birdsApiService: BirdsApiService,
    private activatedRoute: ActivatedRoute,
    private _location: Location
  ) { }

  /*
  get bird data from service
   */
  getBirdData(){
    this.spinnerBirdOn = true;
    this.birdsApiService.getBirdById(this.id)
      .subscribe(data => {
        if( data.status == 'KO' ) {
          alert("SERVER ERROR:\nServer says:\n"+ data.message);
        } else this.bird = data[0];

        this.spinnerBirdOn = false;
      },
      err => {
        alert("ERROR IN REQUEST TO SERVER:\n"+ err);
        this.spinnerBirdOn = false;
      }
    );
  }

  /*
  back button click function
   */
  backClicked() {
    this._location.back();
  }

  // Gets data at init
  ngOnInit() {
    this._subscription = this.activatedRoute.params.subscribe(params => {
      // Recuperamos de la url el id del contacto
      this.id = +params['id']; // (+) convierte string 'id' a number
      // Recuperamos los datos del contacto
      this.getBirdData();
    });
  }
}
